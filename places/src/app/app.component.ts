import { TotalService } from './services/total.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Places to visit';
  total: number;

  constructor(
    private totalService: TotalService
  ) {
    this.totalService.total.subscribe(total => this.total = total);
  }
}
