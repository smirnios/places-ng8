import { ImageLoaderComponent } from './components/image-loader/image-loader.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  MatButtonModule, MatListModule, MatInputModule,
  MatCardModule, MatProgressSpinnerModule, MatIconModule,
  MatGridListModule, MatSnackBarModule, MatToolbarModule } from '@angular/material';

@NgModule({
  declarations: [
    ImageLoaderComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatListModule,
    MatInputModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatGridListModule,
    MatSnackBarModule,
    MatToolbarModule
  ],
  exports: [
    MatButtonModule,
    MatListModule,
    MatInputModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatGridListModule,
    MatSnackBarModule,
    MatToolbarModule,
    ImageLoaderComponent
  ]
})
export class SharedModule { }
