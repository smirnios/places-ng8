import { PlaceService } from 'src/app/services/place.service';
import { Component, OnInit, Input } from '@angular/core';
import { Place } from 'src/app/model/place';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  title = 'Places';
  topPlaces: Place[];

  constructor(private placeService: PlaceService) { }


  ngOnInit() {

    this.placeService.getPlaces().subscribe(
      places => {
        this.topPlaces = places.concat().sort((a, b) => +a.visits < +b.visits ? 1 : -1);
      }
    );
  }
}
