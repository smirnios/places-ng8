import { TotalService } from './../../services/total.service';
import { Component, OnInit, Input } from '@angular/core';
import { Place } from 'src/app/model/place';
import { PlaceService } from 'src/app/services/place.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-place-detail',
  templateUrl: './place-detail.component.html',
  styleUrls: ['./place-detail.component.css']
})
export class PlaceDetailComponent implements OnInit {

  place: Place;
  oldVisits: number;

  constructor(
    private placeService: PlaceService,
    private totalService: TotalService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {

    this.route.paramMap.subscribe(paramMap => {
      const id = paramMap.get('id');
      if ( id ) {
        this.placeService.getPlace(Number(id))
          .subscribe(place => {
            this.place = place;
            this.oldVisits = place.visits;
          });
      } else {
        this.place = new Place();
        this.oldVisits = 0;
      }
    });
  }

  save() {
    this.placeService.createOrUpdatePlace(this.place)
      .subscribe(result => {
        this.place = result;
        this.openSnackBar();
      });
  }

  changeEvent(newValue) {

    if (newValue > this.oldVisits) {
      this.totalService.totalChanged(newValue - this.oldVisits, 'ADD');
    } else if (newValue < this.oldVisits) {
      this.totalService.totalChanged(this.oldVisits - newValue, 'SUBSTRACT');
    }

    this.oldVisits = newValue;
  }

  delete() {
    this.placeService.deletePlace(this.place)
      .subscribe(result => {
        this.place = null;
        this.openSnackBar();
      });
  }

  openSnackBar() {
    this.snackBar.open('Success!', 'OK', {
      duration: 3000
    });
  }

}
