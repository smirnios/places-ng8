import { Component, OnInit } from '@angular/core';
import { Place } from 'src/app/model/place';
import { PlaceService } from 'src/app/services/place.service';

@Component({
  selector: 'app-places',
  templateUrl: './places.component.html',
  styleUrls: ['./places.component.css']
})
export class PlacesComponent implements OnInit {

  constructor(private placeService: PlaceService) { }

  places: Place[];

  selectedPlace: Place;

  ngOnInit() {
    this.getPlaces();
  }

  getPlaces(){
    this.placeService.getPlaces().subscribe(places => {
      this.places = places;
    });

  }
}
