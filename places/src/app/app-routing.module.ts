import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PlacesComponent } from './components/places/places.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { PlaceDetailComponent } from './components/place-detail/place-detail.component';

const routes: Routes = [
  { path: 'places',
    component: PlacesComponent,
    children: [
      { path: 'create', component: PlaceDetailComponent },
      { path: ':id', component: PlaceDetailComponent }
    ]
  },
  { path: 'welcome', component: WelcomeComponent },
  { path: '',   redirectTo: '/welcome', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
