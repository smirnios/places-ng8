import { Injectable } from '@angular/core';
import { Place } from '../model/place';
import { Observable, of, Subject } from 'rxjs';
import { delay, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PlaceService {

  placesUrl = 'http://localhost:3000/places';

  getPlaces$: Subject<Place[]>;

  constructor(private httpClient: HttpClient) {
    this.getPlaces$ = new Subject();
  }

  getPlaces(): Observable<Place[]> {
    this.httpClient.get<Place[]>(this.placesUrl).subscribe(val => {
      this.getPlaces$.next(val);
    });
    return this.getPlaces$;
  }

  getPlace(id: number): Observable<Place> {
    return this.httpClient.get<Place>(this.placesUrl + '/' + id);
  }

  updatePlace(place: Place): Observable<Place> {
    return this.httpClient.put<Place>(this.placesUrl + '/' + place.id, place).pipe(
      tap(val => this.getPlaces())
    );
  }

  createPlace(place: Place): Observable<Place> {
    return this.httpClient.post<Place>(this.placesUrl, place).pipe(
      tap(val => this.getPlaces())
    );
  }

  deletePlace(place: Place): Observable<Place> {
    return this.httpClient.delete<Place>(this.placesUrl + '/' + place.id).pipe(
      tap(val => this.getPlaces())
    );
  }

  createOrUpdatePlace(place: Place): Observable<Place> {
    if (place.id) {
      return this.updatePlace(place);
    } else {
      return this.createPlace(place);
    }
  }

}
