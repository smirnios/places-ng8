import { Injectable } from '@angular/core';
import { BehaviorSubject, of, Observable } from 'rxjs';
import { PlaceService } from './place.service';
import { Place } from '../model/place';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TotalService {

  private _total: BehaviorSubject<number> = new BehaviorSubject(0);

    constructor(private placeService: PlaceService) {
        this.loadInitialData();
    }

    get total() {
        return this._total.asObservable();
    }

    loadInitialData() {
      this.placeService.getPlaces().pipe(
        first()
      ).subscribe(
          (res: Place[]) => {
              const sum =
                res
                  .map(place => place.visits)
                  .reduce((total, visits) => Number(total) + Number(visits), 0);
              this._total.next(sum);
          }
      );
    }

    totalChanged(visits: number, action: string) {
      if (action === 'ADD') {
        this._total.next(this._total.getValue() + visits);
      } else if (action === 'SUBSTRACT') {
        this._total.next(this._total.getValue() - visits);
      }
    }
}

